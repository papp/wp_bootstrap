<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<script src="library/jquery/jquery-2.2.4.js" crossorigin="anonymous"></script>
		{foreach from=$D.LIBRARY.JS item=JS key=kJS}
			{if $JS.ACTIVE}
				{foreach from=$JS.FILE item=FL key=kFL}
				<script src="{$FL}" crossorigin="anonymous" async='true'></script>
				{/foreach}
			{/if}
		{/foreach}
		{foreach from=$D.LIBRARY.CSS item=CSS}
			{if $CSS.ACTIVE}
				{foreach from=$CSS.FILE item=FL key=kFL}
				<link rel="stylesheet" href="{$FL}" crossorigin="anonymous">
				{/foreach}
			{/if}
		{/foreach}
	</head>
